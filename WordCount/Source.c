#include <stdio.h>
#include "Word.h"

int main(int argc, char** argv) {
	int len = 0;
	char filname[FILENAME_MAX] = { 0 };
	FILE *fp;
	char buff[255];
	if (argc < 2) {
		printf("WordCount.exe filename");
		return 1;
	}
	strcpy_s(filname,FILENAME_MAX, argv[1]);
	fopen_s(&fp,filname, "r");
	if (fp) {
		while (fgets(buff, 255, fp)) {
			lineParser(buff);
		}
	}
	len = get_len();
	printf("%d", len);
	free_list();
	getchar();
	return 0;
}
