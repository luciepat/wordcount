#pragma once
#include <string.h>
#include <stdio.h>
#define LONGEST_ENGLISH_WORD_LEN 45
#define NUMBER_OF_CONTRCTIONS 117
#define ASCII_APOSTROPHE 39
#define ASCII_A 65
#define ASCII_Z 90
#define ASCII_a 97
#define ASCII_z 122
int lineParser(const char* line);
void wordAdder(const char* word);
int wordCheck(const char* word);
void addToList(const char* word);
void contraction_deal(const char* word);
void free_list();
int get_len();
