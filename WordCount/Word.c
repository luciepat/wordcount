#include "Word.h"


char** word_array = NULL;
int len = 0;

/*
splits a line by the spaces and sends it to wordAdder
input: line
output: unique words in line
*/
int lineParser(const char * line)
{
	const char* temp_line = line;
	char* space_loc = NULL;
	char word[LONGEST_ENGLISH_WORD_LEN+1] = {0};
	char cleaner[LONGEST_ENGLISH_WORD_LEN + 1] = { 0 };
	do {
		space_loc = memchr(temp_line, ' ', strlen(temp_line));
		if (space_loc) {
			strncpy_s(word, (space_loc - temp_line), temp_line, (space_loc - temp_line));
			word[(space_loc - temp_line)] = 0;//Ingnoring previous longer string
			temp_line = space_loc + 1;
		}
		else {
			strcpy_s(word, (size_t)LONGEST_ENGLISH_WORD_LEN ,temp_line);
			word[strlen(temp_line)] = 0;
		}
		wordAdder(word);
	} while (space_loc);
	return len;
}

/*
Adds unique words to the array
input: word
output: none
*/
void wordAdder(const char * word)
{
	int check = wordCheck(word); //1 if legal word, 2 if contraction 0 else
	if(check == 1) {
		addToList(word);
	}
	else if (check == 2) {
		contraction_deal(word);
	}
}

/*
checks to see if the word is legal
in: word
out: 1-legal 2-contraction 0-illegal
*/
int wordCheck(const char * word)
{
	for (int i = 0; i < strlen(word); i++) {// if is apostrophy
		if (word[i] == ASCII_APOSTROPHE) {
			return 2;
		}
		else if (!((ASCII_A <= word[i] && word[i] <= ASCII_Z) || (ASCII_a <= word[i] && word[i] <= ASCII_z)))//if char is letter
		{
			return 0;
		}
	}
	return 1;
}

/*
if not already in list add to list and add to length
in: word
out: none
*/
void addToList(const char * word)
{
	for (int i = 0; i < len; i++) {
		if (!strcmp(word, word_array[i]))
		{
			return;
		}
	}
	word_array = (char**)realloc(word_array, (len+1)*sizeof(char*));
	if (word_array == NULL) {
		exit(1);
	}
	word_array[len] = (char*)malloc(sizeof(word));
	if (word_array[len] == NULL) {
		for (size_t i = len; i >= 0; i--)
		{
			free(word_array[len]);
		}
		free(word_array);
		exit(1);
	}
	strcpy_s(word_array[len], (size_t)LONGEST_ENGLISH_WORD_LEN, word);
	len++;
}


/*
turns contractions into their base words
in: word
out: none
*/
void contraction_deal(const char * word)
{
	char* contractions[] = { "ain't","aren't","can't","can't've","'cause","could've","couldn't","couldn't've","didn't","doesn't",
							 "don't","hadn't","hadn't've","hasn't","haven't","he'd","he'd've","he'll","he'll've","he's","how'd",
		                     "how'd'y","how'll","how's","I'd","I'd've","I'll","I'll've","I'm","I've","isn't","it'd","it'd've",
		                     "it'll","it'll've","it's","let's","ma'am","mayn't","might've","mightn't","mightn't've","must've",
		                      "mustn't","mustn't've","needn't","needn't've","o'clock","oughtn't","oughtn't've","shan't","sha'n't",
		                      "shan't've","she'd","she'd've","she'll","she'll've","she's","should've","shouldn't","shouldn't've",
		                      "so've","so's","that'd","that'd've","that's","there'd","there'd've","there's","they'd","they'd've",
		                      "they'll","they'll've","they're","they've","to've","wasn't","we'd","we'd've","we'll","we'll've",
		                      "we're","we've","weren't","what'll","what'll've","what're","what's","what've","when's","when've",
		                      "where'd","where's","where've","who'll","who'll've","who's","who've","why's","why've","will've",
		                      "won't","won't've","would've","wouldn't","wouldn't've","y'all","y'all'd","y'all'd've","y'all're",
		                      "y'all've","you'd","you'd've","you'll","you'll've","you're","you've" };
	
	char* meanings[] = { "am not are not is not has not have not","are not am not","can not","cannot have","because","could have",
		                 "could not","could not have","did not","does not","do not","had not","had not have","has not","have not",
		                 "he had he would","he would have","he shall he will","he shall have he will have","he has he is","how did",
		                 "how do you","how will","how has how is how does","I had I would","I would have","I shall I will",
		                 "I shall have I will have","I am","I have","is not","it had it would","it would have","it shall it will",
		                 "it shall have it will have","it has it is","let us","madam","may not","might have","might not",
		                 "might not have","must have","must not","must not have","need not","need not have","of the clock",
		                 "ought not","ought not have","shall not","shall not","shall not have","she had she would","she would have",
		                 "she shall she will","she shall have she will have","she has she is","should have","should not",
		                 "should not have","so have","so as so is","that would that had","that would have","that has that is",
		                 "there had there would","there would have","there has there is","they had they would","they would have",
		                 "they shall they will","they shall have they will have","they are","they have","to have","was not",
		                 "we had we would","we would have","we will","we will have","we are","we have","were not",
		                 "what shall what will","what shall have what will have","what are","what has what is","what have",
		                 "when has when is","when have","where did","where has where is","where have","who shall who will",
		                 "who shall have who will have","who has who is","who have","why has why is","why have","will have","will not",
		                 "will not have","would have","would not","would not have","you all","you all would","you all would have",
		                 "you all are","you all have","you had you would","you would have","you shall you will",
		                 "you shall have you will have","you are","you have" };
	for (int i = 0; i < NUMBER_OF_CONTRCTIONS; i++) {
		if (!strcmp(word, contractions[i]))
		{
			lineParser(meanings[i]);
		}
	}

}

void free_list()
{
	for (int i = len; i >= 0; i--)
	{
		free(word_array[len]);
	}
	free(word_array);
}

int get_len()
{
	return len;
}
